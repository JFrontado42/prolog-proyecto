hombre("Jose").
hombre("Orlando").
hombre("Humberto").
hombre("Jorge").
hombre("Oscar").
hombre("Fernando").
hombre("Alvaro").
hombre("Jaime").
hombre("Edwin").
hombre("JorgeA").
hombre("Andres").
hombre("JuanM").
hombre("Santiago").
hombre("Camilo").
hombre("JuanP").
hombre("Mateo").
mujer("Margarita").
mujer("Rosalba").
mujer("Estella").
mujer("Gladis").
mujer("Marta").
mujer("Patricia").
mujer("Paula").
mujer("Elizabeth").
mujer("Berta").
mujer("DianaM").
mujer("Natalia").
padre("Jose","Humberto").
padre("Jose","Orlando").
padre("Jose","Jorge").
padre("Jose","Oscar").
padre("Jose","Fernando").
padre("Jose","Alvaro").
padre("Jose","Jaime").
padre("Jose","Margarita").
padre("Jose","Rosalba").
padre("Berta","Humberto").
padre("Jose","Estella").
padre("Jose","Galdis").
padre("Jose","Marta").
padre("Jose","Patricia").
padre("Berta","Orlando").
padre("Berta","Jorge").
padre("Berta","Oscar").
padre("Berta","Fernando").
padre("Berta","Alvaro").
padre("Berta","Jaime").
padre("Orlando","Elizabeth").
padre("Orlando","DianaM").
padre("Orlando","Andres").
padre("Humberto","Edwin").
padre("Humberto","Paula").
padre("Margarita","JorgeA").
padre("Rosalba","Liliana").
padre("Liliana","Camilo").
padre("Edwin","Mateo").
padre("Marta","JuanM").
padre("Marta","JuanP").
padre("Jorge","Natalia").
diferente(A,B):-not(A=B).
hijo(A,B):-padre(B,A),hombre(B).
hija(A,B):-padre(B,A),mujer(A).
madre(A,B):-padre(A,B),mujer(A).
nieto(A,B):-padre(B,C),padre(C,A),hombre(A).
hermano(A,B):-padre(C,A),padre(C,B),diferente(A,B).
primo(A,B):-padre(C,A),padre(D,B),hermano(C,D),diferente(D,C).
tio(A,B):-padre(C,B),hermano(C,A),hombre(A).
abuelo(A,B):-nieto(B,A).
tia(A,B):-padre(C,B),hermano(C,A),mujer(A).
sobrino(A,B):-hijo(A,C),hermano(C,B),hombre(A).
sobrina(A,B):-hija(A,C),hermano(C,B),mujer(A).
esposos(A,B):-hijo(C,A),hijo(C,B);hija(D,A),hija(D,B).
sobrinos(A,B):-sobrina(A,B);sobrino(A,B).
bisnieto(A,B):-padre(C,A),padre(D,C),padre(D,B).









