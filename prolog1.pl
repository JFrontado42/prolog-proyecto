pistola(alan).
pistola(luis).
pistola(lucas).

motivo(pablo).
motivo(lucas).
motivo(bernardo).
motivo(luis).
motivo(alan).

coartada_m(lucas, bernardo).
coartada_m(pablo, bernardo).
coartada_m(luis, lucas).

nd_confianza(alan).

coartada_a(X):- coartada_m(X,Y),not(nd_confianza(Y)).

culpable(X):- pistola(X), motivo(X),not(coartada_a(X)).
:- culpable(X), write('El Culpable es '), write(X), nl.
